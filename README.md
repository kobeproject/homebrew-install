## 使用方法

> Homebrew 官方使用 Github 做为镜像仓库，但我们访问不了 githubusercontent 的域名，所以安装不了。下面的命令使用的是清华大学的镜像仓库，安装速度是还可以

### 安装

```shell
/bin/bash -c "$(curl -fsSL https://gitee.com/iamhefang/homebrew-install/raw/master/install.sh)"
```

### 卸载

```shell
/bin/bash -c "$(curl -fsSL https://gitee.com/iamhefang/homebrew-install/raw/master/uninstall.sh)"
```
